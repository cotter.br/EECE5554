# Lab1 Submission

----

Structure of repo:

### /src

Directory that contains all relevant files for Lab1

### /src/Gps-driver

Contains ROS publisher and subscriber. Pubslisher contains gps driver code (Found under /scripts). Also contains launch and custom message files for ROS nodes

### /src/Report.pdf

Written report discussing analyusis of GPS results

### /src/Analysis scripts

Scripts (Python) used to plot results for GPS data post processing

### /src/data 

Data collected in the form of rosbag .bag files


