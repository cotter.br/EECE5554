#!/usr/bin/env python
# coding: utf-8

"""
Post-process GPS stationary data.
rosbag data is imported using pandas and converted to a numpy array. 
matplot lib is used to plot statistics on GPS data
"""

import bagpy
from bagpy import bagreader
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

b = bagreader('outside_static.bag')

csv_data = b.message_by_topic('GNSS_Data')
df_data = pd.read_csv(csv_data)

print('Data Fields: ')
print(df_data.columns.values[1:].tolist())


# lets try to look at the zone:
zone_arr = df_data[["zone"]].to_numpy()
letter_arr = df_data[["letter"]].to_numpy()

print('Zone:')
print(zone_arr[0])

print('Letter:')
print(letter_arr[0])


#############################################
# Convert to format we want
#############################################
secs_arr = df_data[["header.stamp.secs"]].to_numpy()
utm_north_arr = df_data[["utm_north"]].to_numpy()
utm_east_arr = df_data[["utm_east"]].to_numpy()
alt_arr = df_data[["alt"]].to_numpy()
print('Collected %d total data points' % len(secs_arr))

# Get the relative values by subtracting off the first data value
utm_north_arr_rel = utm_north_arr - utm_north_arr[0]
utm_east_arr_rel = utm_east_arr - utm_east_arr[0]
secs_arr_rel = secs_arr - secs_arr[0]
#alt_arr_rel = alt_arr - alt_arr[0]

# Convert to true 1D numpy arrays
utm_north_arr_rel = np.array(utm_north_arr_rel).flatten()
utm_east_arr_rel = np.array(utm_east_arr_rel).flatten()
secs_arr_rel = np.array(secs_arr_rel).flatten()
alt_arr = np.array(alt_arr).flatten()

#############################################
# Plot Static Data
#############################################

# Plot UTM X and Y 
plt.scatter(utm_east_arr_rel, utm_north_arr_rel)
plt.xlabel('UTM Easting (M)')
plt.ylabel('UTM Northing (M)')
plt.title('UTM East to North Stationary')
plt.show()


plt.plot(secs_arr_rel, alt_arr)
plt.xlabel('Time (Sec)')
plt.ylabel('Altitude (M)')
plt.title('Altitude Stationary')
plt.text(50,30, 'Expected Altitude: 6m')
plt.show()

# Actual elevation is 6m 
print("Closest measured value: {}".format(min(alt_arr)))
print(secs_arr_rel[-1])
