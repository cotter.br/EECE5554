#!/usr/bin/env python
"""
GPS Data subscriber
Sits around and reads GPS data
"""
import rospy
from std_msgs.msg import String
from ros_tutorial.msg import GNSS


def callback(data):
    rospy.loginfo(" Zone: %d, Letter %s" % (data.zone, data.letter))

def gpsSub():
    rospy.init_node('GNSS_Sub', anonymous=True)
    rospy.Subscriber('GNSS_Data', GNSS, callback)
    rospy.spin()

if __name__ == '__main__':
    gpsSub()
