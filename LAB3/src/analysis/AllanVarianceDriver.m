%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
File: AllanVarianceDriver.m

Desc: Driver code to call code provided to us int eh lab manual:
https://www.mathworks.com/help/nav/ug/inertial-sensor-noise-analysis-using-allan-variance.html

Create 6 Allan variance plots (3 for gyro, 3 for acceleromter), and show
some noise / error statistics for each
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clc; clear; close all;
rosbag_all = rosbag('fivehours.bag');

% Slight mistake with first 30min so chop off from the datset
start = rosbag_all.StartTime;
endd = rosbag_all.EndTime;
bSel = select(rosbag_all,"Time",[start+30*60, endd],'Topic','/vn/imu');
ros_cell = readMessages(bSel,'DataFormat','struct');

%%
fprintf('Collected %u total messages over 7.5 hours\n', bSel.NumMessages);

% From our ros cell extract the specific field we want into arrays
imu_accel.X = cellfun(@(m) double(m.LinearAcceleration.X), ros_cell);
imu_accel.Y = cellfun(@(m) double(m.LinearAcceleration.X), ros_cell);
imu_accel.Z = cellfun(@(m) double(m.LinearAcceleration.X), ros_cell);

% Do the same for gyroscope readings
imu_gyro.X = cellfun(@(m) double(m.AngularVelocity.X), ros_cell);
imu_gyro.Y = cellfun(@(m) double(m.AngularVelocity.Y), ros_cell);
imu_gyro.Z = cellfun(@(m) double(m.AngularVelocity.Z), ros_cell);

plotAllanVariance(imu_accel.X, 'Acceleromter Data X');
plotAllanVariance(imu_accel.X, 'Acceleromter Data Y');
plotAllanVariance(imu_accel.X, 'Acceleromter Data Z');

plotAllanVariance(imu_gyro.X, 'Gyro Data X');
plotAllanVariance(imu_gyro.Y, 'Gyro Data Y');
plotAllanVariance(imu_gyro.Z, 'Gyro Data Z');