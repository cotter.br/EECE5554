%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
File: AnalyzeIMUData.m

Desc: Create plots and print statistics on VN100 IMU error. 
This file shows error for the small sample size dataset. Short term
error not Allan Variance.

%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc; clear; close all;

% Open data saved in rosbag
rosbag_all = rosbag('basement_short.bag');

% Split up by msg type
imu_sel= select(rosbag_all,'Topic','/imu/imu');
magF_sel= select(rosbag_all,'Topic','/magF/magF');

fprintf('Collected %u IMU messages and %u Magnetic Field message\n', imu_sel.NumMessages, imu_sel.NumMessages);

% Convert rosbag data into matlab cell types
imu_ros_cell = readMessages(imu_sel,'DataFormat','struct');
magF_ros_cell = readMessages(magF_sel,'DataFormat','struct');

% Convert to a struct format that's easy to plot 
imu_data = getIMUDataRelative(imu_ros_cell);
magF_data = getMagFieldDataRelative(magF_ros_cell);

% Plot each respective array
plotXYZ(imu_data.att, 'Orientation Output Error (Yaw, Pitch, Roll)');
plotXYZ(imu_data.gyro, 'Gyro Readings Error (Angular Velocity)');
plotXYZ(imu_data.accel, 'Acceleromter Readings Error');
plotXYZ(magF_data, 'Magnetometer Readings Error');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
Func: getIMUData()
Desc: Convert matlab cell type of IMU data into an a struct with arrays 
thats a little nicer to work with.
Because in this lab we just care about the noise subtract the first value 
off of each array to get the relative noise.
%}
function imu_data = getIMUDataRelative(ros_cell)

% Get timing data
imu_data.sec = cellfun(@(m) double(m.Header.Stamp.Sec), ros_cell);
imu_data.sec = imu_data.sec - imu_data.sec(1);

% Get the quaternion data (3 vectors for XYZ + 1 scalar)
quat.X = cellfun(@(m) double(m.Orientation.X), ros_cell);
quat.Y = cellfun(@(m) double(m.Orientation.Y), ros_cell);
quat.Z = cellfun(@(m) double(m.Orientation.Z), ros_cell);
quat.S = cellfun(@(m) double(m.Orientation.W), ros_cell);

% Use the built in matlab quaternion type so we can convert back to euler
q = quaternion(quat.S, quat.X, quat.Y, quat.Z);

% Convert back to euler angles paying attention to the ordering
euler_angles = quat2eul(q, 'ZYX'); 

imu_data.att.X = euler_angles(:,3);
imu_data.att.X = imu_data.att.X - imu_data.att.X(1);
imu_data.att.Y = euler_angles(:,2);
imu_data.att.Y = imu_data.att.Y - imu_data.att.Y(1);
imu_data.att.Z = euler_angles(:,1);
imu_data.att.Z = imu_data.att.Z - imu_data.att.Z(1);
imu_data.att.units = 'deg';

% Get the 3 accelerometer outputs
imu_data.accel.X = cellfun(@(m) double(m.LinearAcceleration.X), ros_cell);
imu_data.accel.X = imu_data.accel.X - imu_data.accel.X(1); 
imu_data.accel.Y = cellfun(@(m) double(m.LinearAcceleration.Y), ros_cell);
imu_data.accel.Y = imu_data.accel.Y - imu_data.accel.Y(1);
imu_data.accel.Z = cellfun(@(m) double(m.LinearAcceleration.Z), ros_cell);
imu_data.accel.Z = imu_data.accel.Z - imu_data.accel.Z(1);
imu_data.accel.units = 'm/s^2';

% Get the 3 gyro outputs
imu_data.gyro.X = cellfun(@(m) double(m.AngularVelocity.X), ros_cell);
imu_data.gyro.X = imu_data.gyro.X - imu_data.gyro.X(1);
imu_data.gyro.Y = cellfun(@(m) double(m.AngularVelocity.Y), ros_cell);
imu_data.gyro.Y = imu_data.gyro.Y - imu_data.gyro.Y(1);
imu_data.gyro.Z = cellfun(@(m) double(m.AngularVelocity.Z), ros_cell);
imu_data.gyro.Z = imu_data.gyro.Z - imu_data.gyro.Z(1);
imu_data.gyro.units = 'rad/s';
end


%{
Func: getMagFieldData()
Desc: Convert matlab cell type of magnetic field data into an a struct with
arrays thats a little nicer to work with.
Because in this lab we just care about the noise subtract the first value 
off of each array to get the relative noise. 
%}
function magF_data = getMagFieldDataRelative(ros_cell)

% Get timing data
magF_data.sec = cellfun(@(m) double(m.Header.Stamp.Sec), ros_cell);

% Get the 3 magnetic field outputs
magF_data.X = cellfun(@(m) double(m.MagneticField_.X), ros_cell);
magF_data.Y = cellfun(@(m) double(m.MagneticField_.Y), ros_cell);
magF_data.Z = cellfun(@(m) double(m.MagneticField_.Z), ros_cell);
magF_data.units = 'Gauss';
end


%{
Plot 3 axis data in one line  
%}
function plotXYZ(data, title_str)

fprintf('%s Stats\n', title_str);
fprintf('--------------------------------------\n');
fprintf('MeanX: %f\n', mean(data.X)); 
fprintf('StdX : %f\n', std(data.X));
fprintf('--------------------------------------\n');
fprintf('MeanY: %f\n', mean(data.Y)); 
fprintf('StdY : %f\n', std(data.Y));
fprintf('--------------------------------------\n');
fprintf('MeanZ: %f\n', mean(data.Z)); 
fprintf('StdZ : %f\n', std(data.Z));
fprintf('--------------------------------------\n\n\n');

figure()
subplot(3, 1, 1)
plot(data.X)
title(title_str)
xlabel('Sample')
ylabel(data.units)
legend('X')
hold on

subplot(3, 1, 2)
plot(data.Y, 'g')
xlabel('Sample')
ylabel(data.units)
legend('Y')
hold on

subplot(3, 1, 3)
plot(data.Z, 'r')
xlabel('Sample')
legend('Z')
ylabel(data.units)

end
