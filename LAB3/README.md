# Lab 3 Submission

----

Structure of folder:

## /src

Contains ROS publisher source code. Publisher contains IMU driver code. 

## /src/analysis

 - Scripts (MATLAB) used to plot results for IMU data post processing
 - Written report discussing analysis of results

 ## /src/analysis/data

Copy of the data in the form of rosbag .bag files
