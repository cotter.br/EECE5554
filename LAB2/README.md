# Lab2 Submission

----

Structure of repo:

### /src

Directory that contains all relevant files for Lab2

### /src/Gps-driver

Contains ROS publisher and subscriber. Pubslisher contains gps driver code. Also contains launch and custom message files for ROS nodes

### /src/Gps-driver/anlalysis

 - Scripts (Python) used to plot results for GPS data post processing
 - Written report discussing analysis of GPS results

### /src/Gps-driver/data 

Data collected in the form of rosbag .bag files
