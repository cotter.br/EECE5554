#!/usr/bin/env python
# coding: utf-8

"""
File: analyze_walking.py

Post-process GPS walking data from Lab2 data collection. 
rosbag data is imported using pandas and converted to a numpy array. 
matplot lib is used to plot statistics on GPS data
"""

import bagpy
from bagpy import bagreader
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

def analyzeWalkingData(input_fname):

    b = bagreader(input_fname)
    data_name = input_fname[8:-4]

    csv_data = b.message_by_topic('/gps/gps')
    df_data = pd.read_csv(csv_data)

    print('Data Fields: ')
    print(df_data.columns.values[1:].tolist())

    # lets try to look at the zone:
    zone_arr = df_data[["zone"]].to_numpy()
    letter_arr = df_data[["letter"]].to_numpy()
    print('Zone:')
    print(zone_arr[0])
    print('Letter:')
    print(letter_arr[0])


    #############################################
    # Convert to format we want
    #############################################
    secs_arr = df_data[["header.stamp.secs"]].to_numpy()
    lat_arr = df_data[["lat"]].to_numpy()
    lon_arr = df_data[["lon"]].to_numpy()
    utm_north_arr = df_data[["northing"]].to_numpy()
    utm_east_arr = df_data[["easting"]].to_numpy()
    alt_arr = df_data[["alt"]].to_numpy()
    fix_arr = df_data[["fix"]].to_numpy()
    print('Collected %d total data points' % len(secs_arr))

    print('Relative UTM position: [%f, %f]' % (utm_east_arr[0], utm_north_arr[0]))

    # Get the relative values by subtracting off the first data value
    utm_north_arr_rel = utm_north_arr - utm_north_arr[0]
    utm_east_arr_rel = utm_east_arr - utm_east_arr[0]
    secs_arr_rel = secs_arr - secs_arr[0]

    # Convert to true 1D numpy arrays
    lat_arr = np.array(lat_arr).flatten()
    lon_arr = np.array(lon_arr).flatten()
    utm_north_arr_rel = np.array(utm_north_arr_rel).flatten()
    utm_east_arr_rel = np.array(utm_east_arr_rel).flatten()
    secs_arr_rel = np.array(secs_arr_rel).flatten()
    alt_arr = np.array(alt_arr).flatten()
    fix_arr = np.array(fix_arr).flatten()
    
    #############################################
    # Plot Walking data
    #############################################
    expct_fix = 4
    fix_stats = np.all(fix_arr == expct_fix)
    if fix_stats:
        print('Constant fix value: {}. Fixed ambiguity'.format(fix_arr[0]))
    else:
        print('Not constant fix value')
        print(fix_arr)

    if data_name == 'ISEC_Walk':
        # Data was path  walked in the opposite direction so flip it
        utm_east_arr_rel = utm_east_arr_rel[::-1]
        utm_north_arr_rel = utm_north_arr_rel[::-1]

    # Split so we do one side at a time
    end_of_seg_1 = np.argmax(utm_east_arr_rel)
    east_segment_1 = utm_east_arr_rel[0:end_of_seg_1]
    north_segment_1 = utm_north_arr_rel[0:end_of_seg_1]
    a, b = np.polyfit(east_segment_1, north_segment_1, 1)
    best_fit_1 = a*east_segment_1+b
    
    end_of_seg_2 = np.argmax(utm_north_arr_rel)
    east_segment_2 = utm_east_arr_rel[end_of_seg_1:end_of_seg_2]
    north_segment_2 = utm_north_arr_rel[end_of_seg_1:end_of_seg_2]
    a, b = np.polyfit(east_segment_2, north_segment_2, 1)
    best_fit_2 = a*east_segment_2+b

    end_of_seg_3 = np.argmin(utm_east_arr_rel)
    east_segment_3 = utm_east_arr_rel[end_of_seg_2:end_of_seg_3]
    north_segment_3 = utm_north_arr_rel[end_of_seg_2:end_of_seg_3]
    a, b = np.polyfit(east_segment_3, north_segment_3, 1)
    best_fit_3 = a*east_segment_3+b

    east_segment_4 = utm_east_arr_rel[end_of_seg_3:]
    north_segment_4 = utm_north_arr_rel[end_of_seg_3:]
    a, b = np.polyfit(east_segment_4, north_segment_4, 1)
    best_fit_4 = a*east_segment_4+b

    # Combine our best fit segments together
    best_fit = np.concatenate((best_fit_1,best_fit_2,best_fit_3,best_fit_4))
    
    ax = plt.subplot(111)
    ax.plot(utm_east_arr_rel, utm_north_arr_rel, label='Meas.')
    ax.plot(utm_east_arr_rel, best_fit, linestyle='--', linewidth=2, label='Best fit')
    plt.xlabel('UTM Easting (M)')
    plt.ylabel('UTM Northing (M)')
    plt.title('UTM East to North {}'.format(data_name))
    plt.text(70, 5, 'UTM relative to\nEast: {}m\nNorth: {}m '.format(utm_east_arr[0],utm_north_arr[0]))
    ax.legend()
    plt.show()

    # Plot the errors as histograms
    north_error = abs(best_fit - utm_north_arr_rel)
    plt.hist(north_error, bins=25)
    plt.xlabel('Sample Number')
    plt.ylabel('UTM Error (M)')
    plt.title('UTM Error {}'.format(data_name))
    plt.show()

    north_error_mean = np.mean(north_error)
    print('Mean of all UTM Error %f' % north_error_mean)
    north_error_stddev = np.std(north_error)
    print('Std dev of all UTM East terms %f' % north_error_stddev)


    alt_index = np.linspace(1,len(alt_arr), num=len(alt_arr))
    plt.scatter(alt_index, alt_arr)
    plt.xlabel('Sample')
    plt.ylabel('Altitude (M)')
    plt.title('Altitude {}'.format(data_name))
    plt.show()
    

if __name__ == '__main__':
    # Call the function first with our ISEC data
    analyzeWalkingData('../data/ISEC_Walk.bag')

    # Than call again with our roof data
    analyzeWalkingData('../data/Roof_Walk.bag')
