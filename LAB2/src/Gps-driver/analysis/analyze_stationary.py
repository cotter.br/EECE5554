#!/usr/bin/env python
# coding: utf-8

"""

File: analyze_stationary.py

Post-process GPS walking data from Lab2 data collection. 
rosbag data is imported using pandas and converted to a numpy array. 
matplot lib is used to plot statistics on GPS data
"""

import bagpy
from bagpy import bagreader
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


def analyzeStationaryData(input_fname):

    b = bagreader(input_fname)
    data_name = input_fname[8:-4]

    csv_data = b.message_by_topic('/gps/gps')
    df_data = pd.read_csv(csv_data)

    print('Data Fields: ')
    print(df_data.columns.values[1:].tolist())

    # lets try to look at the zone:
    zone_arr = df_data[["zone"]].to_numpy()
    letter_arr = df_data[["letter"]].to_numpy()
    print('Zone:')
    print(zone_arr[0])
    print('Letter:')
    print(letter_arr[0])


    #############################################
    # Convert to format we want
    #############################################
    secs_arr = df_data[["header.stamp.secs"]].to_numpy()
    utm_north_arr = df_data[["northing"]].to_numpy()
    utm_east_arr = df_data[["easting"]].to_numpy()
    alt_arr = df_data[["alt"]].to_numpy()
    fix_arr = df_data[["fix"]].to_numpy()
    print('Collected %d total data points' % len(secs_arr))

    # Get the relative values by subtracting off the first data value
    utm_north_arr_rel = utm_north_arr - utm_north_arr[0]
    utm_east_arr_rel = utm_east_arr - utm_east_arr[0]
    secs_arr_rel = secs_arr - secs_arr[0]

    # Convert to true 1D numpy arrays
    utm_north_arr_rel = np.array(utm_north_arr_rel).flatten()
    utm_east_arr_rel = np.array(utm_east_arr_rel).flatten()
    secs_arr_rel = np.array(secs_arr_rel).flatten()
    alt_arr = np.array(alt_arr).flatten()
    fix_arr = np.array(fix_arr).flatten()

    #############################################
    # Plot Static Data
    #############################################
    expct_fix = 4
    fix_stats = np.all(fix_arr == expct_fix)
    if fix_stats:
        print('Constant fix value: {}. Fixed ambiguity'.format(fix_arr[0]))
    else:
        print('Not constant fix value')
        print(fix_arr)

   
    # Plot UTM X and Y variance from the mean value 
    mean_east = np.mean(utm_east_arr_rel)
    print('Mean of all UTM East terms %f' % mean_east)
    stddev_east = np.std(utm_east_arr_rel)
    print('Std dev of all UTM East terms %f' % stddev_east)

    mean_north = np.mean(utm_north_arr_rel)
    print('Mean of all UTM North terms %f' % mean_north)
    stddev_north = np.std(utm_north_arr_rel)
    print('Std dev of all UTM North terms %f' % stddev_north)
    
    utm_east_var = mean_east - utm_east_arr_rel
    utm_north_var = mean_north - utm_north_arr_rel
    plt.scatter(utm_east_var, utm_north_var)
    plt.xlabel('UTM Easting (M)')
    plt.ylabel('UTM Northing (M)')
    plt.title('UTM East to North {} Variance (Subtracted from mean)'.format(data_name))
    plt.show()

    alt_index = np.linspace(1,len(alt_arr), num=len(alt_arr))
    plt.scatter(alt_index, alt_arr)
    plt.xlabel('Sample')
    plt.ylabel('Altitude (M)')
    plt.title('Altitude {}'.format(data_name))
    plt.show()


if __name__ == '__main__':
    # Call the function first with our ISEC data
    analyzeStationaryData('../data/ISEC_Stationary.bag')

    # Than call again with our roof data
    analyzeStationaryData('../data/Roof_Stationary.bag')
