# Lab4 Submission

### /src

Contains Python ros drivers. Two publishers (GPS and IMU) were used with a launch file to collect data to one large rosbag

### /src/analysis

Contains all MATLAB scripts to post-process for dead reckoning as well as **report.pdf**


