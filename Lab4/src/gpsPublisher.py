#!/usr/bin/env python
"""
GPS-Driver file. 
Parse serial data from GPS puck. 
Publish data over ROS node. 
"""

import rospy
import serial
import utm
from std_msgs.msg import String
from ros_tutorial.msg import GNSS

# Open our serial port to our USB serial device 
# Baud rate = 4800
port = serial.Serial('/dev/ttyUSB0', 4800, timeout=5.0)


def rawLat2Deg(raw_lat):
    """
    Convert latitude provided by serial device
    into form readable by utm lib.
    NOTE: Negative sign added to account for 
    western hemisphere.
    """
    deg = int(raw_lat) // 100
    mins = raw_lat - 100 * deg
    
    return deg + mins/60


def rawLong2Deg(raw_long):
    """
    Convert longitude provided by serial device
    into form readable by utm lib.
    """
    deg = int(-raw_long) // 100
    mins = raw_long - 100 * deg
    
    return deg + mins/60


def gpsDriverPub():
    pub = rospy.Publisher('GNSS_Data', GNSS, queue_size=10)
    rospy.init_node('GNSS_Pub', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    
    # Init custom message type
    gnss = GNSS()
    
    
    while not rospy.is_shutdown():
     
        # Read from serial device to get our GPS data
        line_bytes = port.readline()
        line = str(line_bytes)
        line = line[2:]
        split_line = line.split(',')

        if split_line[0] == '$GPGGA':
            # Only create a new message when we catch the correct format
            gnss.header.seq+=1
            gnss.header.stamp=rospy.Time.now()
            gnss.header.frame_id = "gnss_msg"

            # Parse by field
            lat = float(split_line[2])
            gnss.lat = rawLat2Deg(lat)
            lon = float(split_line[4])
            gnss.lon = rawLong2Deg(lon)
            gnss.alt = float(split_line[9])
            gnss.utm_east, gnss.utm_north, gnss.zone, gnss.letter = utm.from_latlon(gnss.lat, gnss.lon)

            rospy.loginfo(gnss)
            pub.publish(gnss)
        
        rate.sleep()

if __name__ == '__main__':
    try:
        gpsDriverPub()
    except rospy.ROSInterruptException:
        pass
