#!/usr/bin/env python

"""
VN-100 IMU Driver file. Parse serial data into built in ros sensor_msgs messages
for Imu and MagneticField data
"""

import rospy
import serial
from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField
from navpy import angle2quat


port = serial.Serial('/dev/ttyUSB1', 115200, timeout=5.0)

def convertYPR2Quaternion(y,p,r):
   """
   Data from the VN-100 provided as Yaw, Pitch, Roll as (from user guide):
   Euler angle sequence describing the body frame with respect to the local North East Down (NED) frame. 

   This code takes advantage of the NavPy library for this attitude transformation
   https://navpy.readthedocs.io/en/latest/code_docs/attitude_transforms.html

   Default rotation_sequence is already 'ZYX'
   """
   q_scalar, q_vec = angle2quat(y,p,r, input_unit='deg')
   
   return q_scalar, q_vec


def imuDriverPub():

    pub_imu = rospy.Publisher('/imu/imu', Imu, queue_size=10)
    pub_magF = rospy.Publisher('/magF/magF', MagneticField, queue_size=10)
    rospy.init_node('IMU_Pub', anonymous=True)

    imu = Imu()
    magF = MagneticField()

    while not rospy.is_shutdown():
        # Read from serial device to get our GPS data
        line_bytes = port.readline()
        line = str(line_bytes)

        # Remove leading carriage returns and special characters
        # For now lets get rid of it:
        #line = line[4:-6]

        #print('Raw line: %s' % line)
        split_line = line.split(',')

        if split_line[0] == '$VNYMR':

            imu.header.seq+=1
            imu.header.stamp=rospy.Time.now()
            imu.header.frame_id = "imu_msg"

            yaw = float(split_line[1])
            pitch = float(split_line[2])
            roll = float(split_line[3])
            
            q_scalar, q_vec = convertYPR2Quaternion(yaw, pitch, roll)

            imu.orientation.x = q_vec[0]
            imu.orientation.y = q_vec[1]
            imu.orientation.z = q_vec[2]
            imu.orientation.w = q_scalar
            
            imu.angular_velocity.x = float(split_line[10])
            imu.angular_velocity.y = float(split_line[11])
            imu.angular_velocity.z = float(split_line[12])

            imu.linear_acceleration.x = float(split_line[7])
            imu.linear_acceleration.y = float(split_line[8])
            imu.linear_acceleration.z = float(split_line[9])
            
            # Follow similar steps for Magnetic Fields data
            magF.header.seq+=1
            magF.header.stamp=rospy.Time.now()
            magF.header.frame_id = "magF_msg"

            magF.magnetic_field.x = float(split_line[4])
            magF.magnetic_field.y = float(split_line[5])
            magF.magnetic_field.z = float(split_line[6])

            # Every 100 messages print to the screen
            if imu.header.seq % 100 == 0:
               rospy.loginfo(imu)
               rospy.loginfo(magF)
            
            pub_imu.publish(imu)
            pub_magF.publish(magF)



if __name__ == '__main__':
    try:
        imuDriverPub()
    except rospy.ROSInterruptException:
        pass