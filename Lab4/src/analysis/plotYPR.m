%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
Func: plotYPR()

Generic function to plot 3 axis yaw pitch roll data. Mainly used for IMU data.
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotYPR(data, title_str)

figure()
subplot(3, 1, 1)
plot(data.yaw)
title(title_str)
xlabel('Sample')
ylabel(data.units)
legend('Yaw')
hold on

subplot(3, 1, 2)
plot(data.pitch, 'g')
xlabel('Sample')
ylabel(data.units)
legend('Pitch')
hold on

subplot(3, 1, 3)
plot(data.roll, 'r')
xlabel('Sample')
legend('Roll')
ylabel(data.units)

end
