%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
Func: correctMagnetometer.m

Correct measured magnetometer data for hard-iron and soft-iron effects.
Based on content in Lecture 8
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function corrected_data = correctMagnetometer(raw_data)

% Try with the built in magCal() function
raw_data_mat = [raw_data.X, raw_data.Y, raw_data.Z];
[soft_iron_factor, hard_iron_factor, exp_strength] = magcal(raw_data_mat);

fprintf('Expected magnetic field strength: %f (uT)\n', exp_strength);

corrected_data_mat = (raw_data_mat - hard_iron_factor) * soft_iron_factor;
corrected_data.X = corrected_data_mat(:,1);
corrected_data.Y = corrected_data_mat(:,2);
corrected_data.Z = corrected_data_mat(:,3);

figure()
plot(raw_data.X, raw_data.Y)
hold on
plot(corrected_data.X, corrected_data.Y)
axis equal
xlabel('Mx (Gauss)')
ylabel('My (Gauss)')
title('Magnetometer Data')
legend('Raw Data', 'Corrected for Hard and Soft Iron')

end