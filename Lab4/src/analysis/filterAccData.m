
%
%  https://www.mathworks.com/matlabcentral/answers/166890-double-integration-of-the-acceleration-signals-to-obtain-displacment-signals
%

function accel_filt = filterAccData(accel_raw, accel_time)
PLOT_FFT = true;

% Let's look at the data before we do anything to it
figure()
plot(accel_time, accel_raw)
xlabel('Seconds')
ylabel('Acceleration (m/s^2)')
% We know we shoudl have 0 mean data, lets check that
pre_filter_mean = mean(accel_raw);
title_text = sprintf('Raw Accel.X (First leg) [Mean=%f]', pre_filter_mean);
title(title_text)



% Lets also look at normalizing the acc
% I don't think we have to normalize if we are donig all this filtering

% Before 

% After


Fs = 40;            % Sampling frequency                    
%T = 1/Fs;
L = length(accel_raw);

% First we want to look at the FFT of the data
accel_raw_fft = fft(accel_raw);
P2 = abs(accel_raw_fft/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

if PLOT_FFT
figure()
plot(f,P1)
title('Single-Sided Amplitude Spectrum of Acc(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')
end

% Visually inspect FFT data to find the cutoff freq for our low pass filter

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------

accel_filt = lowpass(accel_raw, 0.023, Fs); % 40
%accel_filt = highpass(accel_filt, 0.0307 , 40); % 0.023
%accel_filt = bandpass(accel_raw, [0.07 18], 40); % 1, 0.1

% get all the values under a certian threshold. 
% This is bias that should be at 0
bias_thresh = 0.15;
accel_filt_bias = accel_filt(accel_filt > 0);
accel_filt_bias = accel_filt(accel_filt_bias < bias_thresh);
accel_filt_bias = mean(accel_filt_bias);
accel_filt_bias
% Manually removing the bias
accel_filt = accel_filt - 0.13; %accel_filt_bias;


figure()
plot(accel_time, accel_filt)
xlabel('Seconds')
ylabel('Acceleration (m/s^2)')
% We know we should have 0 mean data, lets check that
filtered_mean = mean(accel_filt);
title_text = sprintf('LPF Accel.X (First leg) [Mean=%f]', filtered_mean);
title(title_text)


end



%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%{
% Sampling frequency
fs = 40;
 
% Nyquist frequency
fn = fs/2;
 
% Band pass filter frequencies [Hz]
fa =  0.02;
fb =  .5;
 
% Normalized frequencies
fa_n = fa / fn; 
fb_n = fb / fn;
 
% Calculate filter coefficients
% Attenuation (dB)
A = 40;
% Bandwidth
% BW = fb-fa;
% N = 2/3 * log10(1/(delta1 * delta2)) * fs/BW;
 
filter_order = 1829;
c = fir1(filter_order, [fa_n, fb_n], "bandpass");
freqz(c,1, 5000, fs)

accel_filt = filter(c, 1, accel_raw);
%}
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
