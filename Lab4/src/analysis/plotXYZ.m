%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
Func: plotXYZ()

Generic function to plot 3 axis XYZ data. Mainly used for IMU data.
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotXYZ(data, title_str)

fprintf('%s Stats\n', title_str);
fprintf('--------------------------------------\n');
fprintf('MeanX: %f\n', mean(data.X)); 
fprintf('StdX : %f\n', std(data.X));
fprintf('--------------------------------------\n');
fprintf('MeanY: %f\n', mean(data.Y)); 
fprintf('StdY : %f\n', std(data.Y));
fprintf('--------------------------------------\n');
fprintf('MeanZ: %f\n', mean(data.Z)); 
fprintf('StdZ : %f\n', std(data.Z));
fprintf('--------------------------------------\n\n\n');

figure()
subplot(3, 1, 1)
plot(data.X)
title(title_str)
xlabel('Sample')
ylabel(data.units)
legend('X')
hold on

subplot(3, 1, 2)
plot(data.Y, 'g')
xlabel('Sample')
ylabel(data.units)
legend('Y')
hold on

subplot(3, 1, 3)
plot(data.Z, 'r')
xlabel('Sample')
legend('Z')
ylabel(data.units)

end