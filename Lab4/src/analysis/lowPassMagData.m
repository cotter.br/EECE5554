%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
Func: lowPassGyroData.m
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function mag_yaw_lpf = lowPassMagData(mag_yaw_unfilt_deg)
PLOT_FFT = false;

% Let's look at the data before we do anything to it
figure()
plot(mag_yaw_unfilt_deg)
xlabel('Seconds')
ylabel('Yaw Angle (deg)')
title('Unfiltered Yaw Angle from Magnitude')

% Let's try unwrapping before we filter
% Convert to rad before we do our math on it 
mag_yaw_unfilt = deg2rad(mag_yaw_unfilt_deg);
mag_yaw_unwrap = unwrap(mag_yaw_unfilt);
figure()
plot(mag_yaw_unwrap)
xlabel('Seconds')
ylabel('Yaw Angle (rad)')
title('Unfiltered Yaw Angle from Magnitude Unwrapped')


Fs = 40; % Sampling frequency     
L = length(mag_yaw_unwrap);

% First we want to look at the FFT of the data
unfilt_fft = fft(mag_yaw_unwrap);
P2 = abs(unfilt_fft/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

if PLOT_FFT
figure()
plot(f,P1)
title('Single-Sided Amplitude Spectrum of Yaw Angle(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')
end

% Visually inspect FFT data to find the cutoff freq for our low pass filter
cutoff_freq = 0.025;
mag_yaw_lpf = lowpass(mag_yaw_unwrap, cutoff_freq, Fs);

% View wrapping at 2pi
mag_yaw_lpf = rad2deg(mag_yaw_lpf);
mag_yaw_lpf = wrapTo180(mag_yaw_lpf);

figure()
plot(mag_yaw_lpf)
xlabel('Sec')
ylabel('Yaw Angle (deg)')
title('LPFd Yaw Angle from Magnitude')

end
