%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
Func: highPassGyroData.m
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gyro_yaw_hpf = highPassGyroData(gyro_yaw_unfilt_deg)
PLOT_FFT = false;

gyro_yaw_hpf = gyro_yaw_unfilt_deg;


% Let's look at the data before we do anything to it
figure()
plot(gyro_yaw_unfilt_deg)
xlabel('Seconds')
ylabel('Yaw Angle (deg)')
title('Unfiltered Yaw Angle from Gyro')


% Let's try unwrapping before we filter
% Convert to rad before we do our math on it ? 
gyro_yaw_unwrap = gyro_yaw_unfilt_deg;

Fs = 40; % Sampling frequency     
L = length(gyro_yaw_unwrap);

% First we want to look at the FFT of the data
unfilt_fft = fft(gyro_yaw_unwrap);
P2 = abs(unfilt_fft/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

if PLOT_FFT
figure()
plot(f,P1)
title('Single-Sided Amplitude Spectrum of Yaw Angle(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')
end

% Visually inspect FFT data to find the cutoff freq for our low pass filter
cutoff_freq = 0.25;
gyro_yaw_hpf = highpass(gyro_yaw_unwrap, cutoff_freq, Fs);

% View wrapping at 2pi
gyro_yaw_hpf = wrapToPi(gyro_yaw_hpf);

figure()
plot(gyro_yaw_hpf)
xlabel('Sec')
ylabel('Yaw Angle (deg)')
title('HPFd Yaw Angle from gyronitude')


end