%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
File: NavStackAnalysis.m

Desc: Create plots and show statistics for Lab4 data run where GPS receiver
and VN-100 IMU where used in tandem on a drive around Northeastern campus.

-----

Align yaw angle to what you are moving to. 

Focus on just teh first leg

We want to get so our GPS and IMU data look like na F shape for the 
first leg 
If at an anglt than that means yaw angle is incorrect

Then add in factors, so full data looks like a smaller scale version of
what we have 
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc; clear; close all;

% Open data saved in rosbag
rosbag_all = rosbag('data/drive_IMU_and_GPS_0310_REDO.bag');

% Split up by ros topic
gps_sel= select(rosbag_all,'Topic','/gps_utm');
imu_sel= select(rosbag_all,'Topic','/imu_data');
mag_sel= select(rosbag_all,'Topic','/magnetometer_data');

% Convert rosbag data into matlab cell type
gps_ros_cell = readMessages(gps_sel,'DataFormat','struct');
imu_ros_cell = readMessages(imu_sel,'DataFormat','struct');
mag_ros_cell = readMessages(mag_sel,'DataFormat','struct');

% Convert from cell to a struct format that's easier to work with 
gps_data_full = getGPSData(gps_ros_cell);
imu_data_full = getIMUData(imu_ros_cell);
mag_data_full = getMagData(mag_ros_cell);

%plotYPR(imu_data_full.att, 'DEBUG Attitude DEBUG');

%%
% Initial setup
close all

% First plot the GPS data of the full drive 
plotUTM(gps_data_full, 'Full drive UTM');

% Grab a subset of the full dataset. Here we use hardcoded values (via 
% visual inspection of the plot) for the starting and ending indexes 
GPS_CIRC_START = 106;
GPS_CIRC_END   = 210;
GPS_LEG1_START = GPS_CIRC_END + 180;
GPS_LEG1_END   = GPS_LEG1_START + 130;

% Create a copy of the full dataset to hold only the ruggles circle data
%gps_data_circle = gps_data_full;
gps_data_circle.utm.easting  = gps_data_full.utm.easting( GPS_CIRC_START : GPS_CIRC_END);
gps_data_circle.utm.northing = gps_data_full.utm.northing(GPS_CIRC_START : GPS_CIRC_END);
gps_data_circle.utm.zone_letter  = gps_data_full.utm.zone_letter;

% Show that we have the correct indexes
plotUTM(gps_data_circle, 'Ruggles Circle UTM');

% Create a copy of the full dataset to hold only the 1st straightaway data
%gps_data_straight = gps_data_full;
gps_data_straight.utm.easting  = gps_data_full.utm.easting( GPS_LEG1_START : GPS_LEG1_END);
gps_data_straight.utm.northing = gps_data_full.utm.northing(GPS_LEG1_START : GPS_LEG1_END);
gps_data_straight.utm.zone_letter  = gps_data_full.utm.zone_letter;

% Show that we have the correct indexes
plotUTM(gps_data_straight, 'First straight away GPS UTM');

%%

% Get the IMU datset indexes by multiplying by 40Hz
IMU_CIRC_START = GPS_CIRC_START * 40;
IMU_CIRC_END   = GPS_CIRC_END   * 40;
IMU_LEG1_START = GPS_LEG1_START * 40;
IMU_LEG1_END = GPS_LEG1_END * 40;

% Create a copy of the full dataset to hold only the ruggles circle data
%imu_data_circle           = imu_data_full;
%imu_data_circle.sec       = imu_data_full.sec(IMU_CIRC_START : IMU_CIRC_END);
imu_data_circle.att.yaw   = imu_data_full.att.yaw(IMU_CIRC_START : IMU_CIRC_END);
imu_data_circle.att.pitch = imu_data_full.att.pitch(IMU_CIRC_START : IMU_CIRC_END);
imu_data_circle.att.roll  = imu_data_full.att.roll(IMU_CIRC_START : IMU_CIRC_END);

imu_data_circle.gyro.X = imu_data_full.gyro.X(IMU_CIRC_START : IMU_CIRC_END);
imu_data_circle.gyro.Y = imu_data_full.gyro.Y(IMU_CIRC_START : IMU_CIRC_END);
imu_data_circle.gyro.Z = imu_data_full.gyro.Z(IMU_CIRC_START : IMU_CIRC_END);

% Create a copy of the full dataset to hold only the ruggles circle data
%mag_data_circle   = imu_data_full;
mag_data_circle.X = mag_data_full.X(IMU_CIRC_START : IMU_CIRC_END);
mag_data_circle.Y = mag_data_full.Y(IMU_CIRC_START : IMU_CIRC_END);
mag_data_circle.Z = mag_data_full.Z(IMU_CIRC_START : IMU_CIRC_END);

%%
% Let's get just the first leg
%imu_data_straight     = imu_data_full;
% TODO: We shoudl do our seconds array custom so we can acutally use them 
%imu_data_straight.sec = imu_data_full.sec(IMU_LEG1_START : IMU_LEG1_END);
imu_data_straight.accel.X = imu_data_full.accel.X(IMU_LEG1_START : IMU_LEG1_END);
imu_data_straight.accel.Y = imu_data_full.accel.Y(IMU_LEG1_START : IMU_LEG1_END);
imu_data_straight.accel.Z = imu_data_full.accel.Z(IMU_LEG1_START : IMU_LEG1_END);
imu_data_straight.accel.units = imu_data_full.accel.units;

imu_data_straight.gyro.X = imu_data_full.gyro.X(IMU_LEG1_START : IMU_LEG1_END);
imu_data_straight.gyro.Y = imu_data_full.gyro.Y(IMU_LEG1_START : IMU_LEG1_END);
imu_data_straight.gyro.Z = imu_data_full.gyro.Z(IMU_LEG1_START : IMU_LEG1_END);

%%

% Lets get the full moving dataset acceleration
imu_full_moving.accel.X = imu_data_full.accel.X(IMU_LEG1_START : end);
imu_full_moving.accel.Y = imu_data_full.accel.Y(IMU_LEG1_START : end);
imu_full_moving.accel.units = imu_data_full.accel.units;

imu_full_moving.gyro.Z = imu_data_full.gyro.Z(IMU_LEG1_START : end);

% Lets get the full GPS position
gps_full_moving.utm.easting  = gps_data_full.utm.easting( GPS_LEG1_START : end);
gps_full_moving.utm.northing = gps_data_full.utm.northing(GPS_LEG1_START : end);


%%
%%%%%%%%%%%%%%%%%%%% PART 1 Estimating Yaw Angle %%%%%%%%%%%%%%%%%%%%%%%%%%

% Correct the magnetometer data for hard-iron and soft-iron effects
% For this calibration use only the ruggles circle data
mag_data_corrected = correctMagnetometer(mag_data_circle);

% Trying to use our corrected magnitude to estimate yaw angle
% Estimate yaw in radians with arc tan of the ratio
angle = mag_data_corrected.Y / mag_data_corrected.X;
yaw_est = atan2(mag_data_corrected.X, mag_data_corrected.Y);

% Show results in degrees 
yaw_angle.from_mag = rad2deg(yaw_est);

% Integrate gyro Y data to estimate yaw angle
circle_length_imu = length(imu_data_circle.gyro.Z);
imu_time_circle = 0:0.0250:circle_length_imu/40;
imu_time_circle = imu_time_circle(1:circle_length_imu);

% Input to integration is rad/s
yaw_angle.from_gyro = cumtrapz(imu_time_circle, imu_data_circle.gyro.Z);
yaw_angle.from_gyro = wrapToPi(yaw_angle.from_gyro);
yaw_angle.from_gyro = rad2deg(yaw_angle.from_gyro);

% So that we can show starting points lining up
yaw_angle.from_gyro_match = yaw_angle.from_gyro(100 : end);

figure();
plot(yaw_angle.from_gyro_match)
hold on
plot(yaw_angle.from_mag)
xlabel('Samples @ 40Hz')
ylabel('Yaw angle (deg)')
title('Estimated Yaw Angle')
legend('Integral of gyro', 'Atan of magnetometer')

%%
% Now, what we really want to do is fuse these 2x datasets 
% to get an even better estimate of yaw angle

% Let's cut off at a nice even 4000 samples for now
yaw_angle.from_gyro_match = yaw_angle.from_gyro_match(1:4000);
yaw_angle.from_mag = yaw_angle.from_mag(1:4000);

% Filter the magnetometer estimate using a low pass filter
comp_filter.low_pass = lowPassMagData(yaw_angle.from_mag);

% Filter the gyro estimate using a high pass filter
comp_filter.high_pass = highPassGyroData(yaw_angle.from_gyro_match);

% Then fuse the 2 together with higher confidence in our gyro data
comp_filter.combined = (0.1 * comp_filter.high_pass) + (0.9 * comp_filter.low_pass);

% Compare this estimate to the IMU yaw angle 
figure();
plot(comp_filter.combined, 'g')
xlabel('Samples @ 40Hz')
ylabel('Yaw angle (deg)')
title('Yaw Angle from complementary filter')

%%%%%%%%%%%%%%%%%%% PART 2 Forward Velocity %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

% ---- Estimate velocity with accelerometer data ----
straight_length_acc = length(imu_full_moving.accel.X);
accel_time = 0:0.0250:straight_length_acc/40;
accel_time = accel_time(1:straight_length_acc);

% Filter our raw data to remove both bias and nosie
%accel_filt = filterAccDataFULL(imu_full_moving.accel.X, accel_time);
full_accl_mean = mean(imu_full_moving.accel.X);
accel_filt = imu_full_moving.accel.X - full_accl_mean;
accel_filt = accel_filt';

% Integrate acceleration to get velocity
fwd_vel.from_accel = cumtrapz(accel_time, accel_filt);

% ---- Estimate velocity with GPS data ----
gps_time  = 1 : length(gps_full_moving.utm.easting);
east_pos  = gps_full_moving.utm.easting;
north_pos = gps_full_moving.utm.northing;
gps_pos_hypot = hypot(east_pos, north_pos);
fwd_vel.from_gps = gradient(gps_pos_hypot)./gradient(gps_time);

max_gps_vel = max(abs(fwd_vel.from_gps(:,1)));

% Get the relative start to match plots
for i = 1 : length(fwd_vel.from_accel)
  
    % Remove large negative velocities even after mean subtraction
    if i  < 100*40
        fwd_vel.from_accel(i) = fwd_vel.from_accel(i) + 5;
    end

    if i  > 100*40 && i < 500*40   
        fwd_vel.from_accel(i) = fwd_vel.from_accel(i) + 3.5;
    end

    % Setting a speed threshold to remove huge velocities
    % I promise I didn't go 60+ mph in the NUance car like  
    % the raw data spike suggests
    if fwd_vel.from_accel(i) > max_gps_vel
        fwd_vel.from_accel(i) = 4; %max_gps_vel;
    end
   
end

figure();
plot(accel_time, fwd_vel.from_accel, 'r')
hold on
plot(gps_time, abs(fwd_vel.from_gps), 'g')
xlabel('Sec')
ylabel('Velocity (m/s)')
title('Estimated Forward velocity')
legend('Integrated accleromter X', 'From GPS')

%%
%%%%%%%%%%%%%%% PART 3 Dead Reckoning %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%

% Multiplication of GryoZ * Velocity X compared to accelerometer Y
gyro_z = imu_full_moving.gyro.Z; %  Units: rad/s 
gyro_z = flattenVec(gyro_z);

velo_x = fwd_vel.from_accel; %  Units: m/s
velo_x = flattenVec(velo_x);

accel_Y.from_est = gyro_z .* velo_x; % Units: m/s^2

% The value straight from the acceleromter.
accel_Y.from_accel = imu_full_moving.accel.Y; % Units: m/s^2
accel_Y.from_accel = flattenVec(accel_Y.from_accel);

figure()
plot(accel_Y.from_accel, 'r')
hold on
plot(accel_Y.from_est, 'g')
xlabel('Sec')
ylabel('Acceleration (m/s^2)')
title('Meas. vs Estimated Y Acceleration')
legend('Meas acceleromter Y', 'Gyro Y * est. velocity X')

est_accel_diff = mean(accel_Y.from_accel - accel_Y.from_est)

%%
% Integrate with ideal time array
drive_length = length(imu_full_moving.gyro.Z);
imu_time_drive = 0 : 0.025 : drive_length/40;
imu_time_drive = imu_time_drive(1:end-1);


starting_angle_rad = deg2rad(-17);
fprintf('Starting angle: %f deg\n', rad2deg(starting_angle_rad));

heading_from_gyro = cumtrapz(imu_time_drive, imu_full_moving.gyro.Z);
heading_from_gyro = heading_from_gyro + starting_angle_rad;

est_vel = fwd_vel.from_accel;
est_vel = flattenVec(est_vel);

heading_from_gyro = flattenVec(heading_from_gyro);

% Lets look at heading to see if it even make sense
heading_from_gyro_asDeg = rad2deg(heading_from_gyro);

figure()
plot(heading_from_gyro_asDeg)
title('Heading from gyro as deg')
ylabel('deg')

pos_length = length(est_vel);

est_pos.X = zeros(1, pos_length);
est_pos.Y = zeros(1, pos_length);

sum.X = 0;
sum.Y = 0;
for i = 1 : pos_length 
    sum.X = sum.X + est_vel(i) * sin(heading_from_gyro(i));
    sum.Y = sum.Y + est_vel(i) * cos(heading_from_gyro(i));

    est_pos.X(i) = sum.X;
    est_pos.Y(i) = sum.Y;
end

% Apply a global scale factor
scale_factor = 50;
est_pos.X = est_pos.X / scale_factor; 
est_pos.Y = est_pos.Y / scale_factor;


figure();
plot(est_pos.X, est_pos.Y) 
xlabel('Pos X (m)')
ylabel('Pos Y (m)')
title('Est Position X vs Y')
legend('Estimated')

% plotting the real east and north from GPS 
gps_east_pos = east_pos;
gps_north_pos = north_pos;

gps_east_pos_rel = gps_east_pos - gps_east_pos(1);
gps_north_pos_rel = gps_north_pos - gps_north_pos(1);

figure();
plot(gps_east_pos_rel, gps_north_pos_rel)
xlabel('Easting Position (m)')
ylabel('Northing Position (m)')
title('RELATIVE Position from GPS')

est_pos.downsample.X = est_pos.X(1 : 40 : end); 
est_pos.downsample.Y = est_pos.Y(1 : 40 : end);


figure();
plot(gps_east_pos_rel, gps_north_pos_rel, 'g')
hold on
plot(est_pos.downsample.X, est_pos.downsample.Y, 'b')
xlabel('Easting Position (m)')
ylabel('Northing Position (m)')
title('Dead Reckoning vs. GPS Position')
legend('GPS', 'Dead Reckoning IMU')






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%{
Func: getIMUData()
Desc: Convert matlab cell type of IMU data into an a struct with arrays 
thats a little nicer to work with.
Because in this lab we just care about the noise subtract the first value 
off of each array to get the relative noise.
%}
function imu_data = getIMUData(ros_cell)

% Get timing data
imu_data.sec = cellfun(@(m) double(m.Header.Stamp.Sec), ros_cell);

% Get the quaternion data (3 vectors for XYZ + 1 scalar)
quat.X = cellfun(@(m) double(m.Orientation.X), ros_cell);
quat.Y = cellfun(@(m) double(m.Orientation.Y), ros_cell);
quat.Z = cellfun(@(m) double(m.Orientation.Z), ros_cell);
quat.S = cellfun(@(m) double(m.Orientation.W), ros_cell);

% Use the built in matlab quaternion type so we can convert back to euler
%q = quaternion(quat.S, quat.X, quat.Y, quat.Z);

q_vec = [quat.S; quat.X; quat.Y; quat.Z];

% We want to pass in a nx4 matrix
q_mat = reshape(q_vec, [], 4);

S_ = quat.S';
X_ = quat.X';
Y_ = quat.Y';
Z_ = quat.Z';

% Convert back to euler angles paying attention to the ordering
[yaw, pitch, roll] = quat2angle(q_mat,'XYZ');

imu_data.att.yaw = yaw; 
imu_data.att.pitch = pitch;
imu_data.att.roll = roll; 
imu_data.att.units = 'deg';

% Get the 3 accelerometer outputs
imu_data.accel.X = cellfun(@(m) double(m.LinearAcceleration.X), ros_cell);
imu_data.accel.Y = cellfun(@(m) double(m.LinearAcceleration.Y), ros_cell);
imu_data.accel.Z = cellfun(@(m) double(m.LinearAcceleration.Z), ros_cell);
imu_data.accel.units = 'm/s^2';

% Get the 3 gyro outputs
imu_data.gyro.X = cellfun(@(m) double(m.AngularVelocity.X), ros_cell);
imu_data.gyro.Y = cellfun(@(m) double(m.AngularVelocity.Y), ros_cell);
imu_data.gyro.Z = cellfun(@(m) double(m.AngularVelocity.Z), ros_cell);
imu_data.gyro.units = 'rad/s';

end



%{
Func: getGPSData()
%}
function gps_data = getGPSData(ros_cell)

% Get timing data
gps_data.sec = cellfun(@(m) double(m.Header.Stamp.Sec), ros_cell);

gps_data.utm.easting = cellfun(@(m) double(m.UtmEasting), ros_cell);
gps_data.utm.northing = cellfun(@(m) double(m.UtmNorthing), ros_cell);

zone = num2str(ros_cell{1,1}.Zone);
letter = ros_cell{1,1}.Letter;
gps_data.utm.zone_letter = strcat(zone, letter);

gps_data.utm.units = 'UTM';

end



%{
Func: getMagData()
Desc: Convert matlab cell type of magnetic field data into an a struct with
arrays thats a little nicer to work with.
Because in this lab we just care about the noise subtract the first value 
off of each array to get the relative noise. 
%}
function mag_data = getMagData(ros_cell)

% Get timing data
mag_data.sec = cellfun(@(m) double(m.Header.Stamp.Sec), ros_cell);

% Get the 3 magnetic field outputs
mag_data.X = cellfun(@(m) double(m.MagneticField_.X), ros_cell);
mag_data.Y = cellfun(@(m) double(m.MagneticField_.Y), ros_cell);
mag_data.Z = cellfun(@(m) double(m.MagneticField_.Z), ros_cell);
mag_data.units = 'Gauss';
end
