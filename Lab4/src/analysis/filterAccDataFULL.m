function accel_filt = filterAccDataFULL(accel_raw, accel_time)
PLOT_FFT = true;

% Let's look at the data before we do anything to it
figure()
plot(accel_time, accel_raw)
xlabel('Seconds')
ylabel('Acceleration (m/s^2)')
% We know we shoudl have 0 mean data, lets check that
pre_filter_mean = mean(accel_raw);
title_text = sprintf('Raw Accel.X (First leg) [Mean=%f]', pre_filter_mean);
title(title_text)



% Lets also look at normalizing the acc
% I don't think we have to normalize if we are donig all this filtering

% Before 

% After


Fs = 40;            % Sampling frequency                    
%T = 1/Fs;
L = length(accel_raw);

% First we want to look at the FFT of the data
accel_raw_fft = fft(accel_raw);
P2 = abs(accel_raw_fft/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

if PLOT_FFT
figure()
plot(f,P1)
title('Single-Sided Amplitude Spectrum of Acc(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')
end

% Visually inspect FFT data to find the cutoff freq for our low pass filter

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------

%accel_filt = lowpass(accel_raw, 0.023, Fs); % 40
%accel_filt = highpass(accel_filt, 0.0307 , 40); % 0.023
%accel_filt = highpass(accel_raw, 0.0307 , 40);
accel_filt = bandpass(accel_raw, [1 1.34], 40); % 1, 0.1

% get all the values under a certian threshold. 
% This is bias that should be at 0
bias_thresh = 0.15;
accel_filt_bias = accel_filt(accel_filt > 0);
accel_filt_bias = accel_filt(accel_filt_bias < bias_thresh);
accel_filt_bias = mean(accel_filt_bias);
accel_filt_bias
% Manually removing the bias
accel_filt = accel_filt - accel_filt_bias;%  - 0.13; %accel_filt_bias;

%{
for i = 1 : length(accel_filt)
    if accel_filt(i) < 0
        accel_filt(i) = 0;
    end
end
%}

figure()
plot(accel_time, accel_filt)
xlabel('Seconds')
ylabel('Acceleration (m/s^2)')
% We know we should have 0 mean data, lets check that
filtered_mean = mean(accel_filt);
title_text = sprintf('LPF Accel.X (First leg) [Mean=%f]', filtered_mean);
title(title_text)


end