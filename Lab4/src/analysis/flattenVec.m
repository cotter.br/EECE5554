%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ros bag returns some vectors as columns.
% I like doing all my math in row
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function vec_out = flattenVec(vec_in)

vec_dim = size(vec_in);

if vec_dim(1) ~= 1
    vec_out = vec_in';
else
    vec_out = vec_in;
end

end