%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
Func: plotUTM.m
Desc: Generic way to plot gps data UTM northing and easting.
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotUTM(gps_data, title_str)

title_str = sprintf('%s [%s]', title_str, gps_data.utm.zone_letter);

figure()
plot(gps_data.utm.easting, gps_data.utm.northing, '.:')
title(title_str)
xlabel('UTM Easting (xxx)')
ylabel('UTM Northing (xxx)')

end

